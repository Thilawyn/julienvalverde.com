config = require "lapis.config"


--
-- Configuration
--
config {"production", "development"},
	port: 8080
	systemd:
		env: false
		user: true

config "production",
	num_workers: 4
	code_cache: "on"

config "development",
	systemd:
		journal: true
