import Widget from require "lapis.html"


--
-- Website Layout
--
class Layout extends Widget
	content: =>
		html_5 ->
			head ->
				meta charset: "utf-8"

				-- CSS
				link rel: "stylesheet", href: "static/css/style.css"
				link rel: "stylesheet", href: "static/css/fonts/bebas_neue/bebas_neue.css"
				link rel: "stylesheet", href: "https://fonts.googleapis.com/css?family=Roboto&display=swap"

				-- JS
				script src: "static/js/sticky_nav.js"

				-- Title
				@title = "Julien Valverdé's Home"

				if @page_title
					title "#{@page_title} • #{@title}"
				else
					title @title

			body ->
				header ->
					div class: "title", ->
						h1 "Julien Valverdé's Home"

					nav ->
						ul ->
							li -> a href: ".", "Home"
							li -> a href: "about/", "About me"
							li -> a href: "projects/", "Projects"
							li -> a href: "https://gitlab.com/Thilawyn", "GitLab"

				main ->
					-- Page content
					div class: "content", ->
						@content_for "inner"

					-- Sidebar
					div class: "sidebar", ->
						p "Hello, this my website. Glad you came!"

				footer ->
