import Widget from require "lapis.html"


--
-- Under Construction Page Layout
--
class UnderConstructionLayout extends Widget
	content: =>
		html_5 ->
			head ->
				meta charset: "utf-8"

				-- CSS
				link rel: "stylesheet", href: "static/css/under_construction.css"
				link rel: "stylesheet", href: "https://fonts.googleapis.com/css?family=Roboto&display=swap"

				-- Title
				title "Julien Valverdé's Home"

			body ->
				div id: "under_construction", ->
					-- Text block
					h1 ->
						text "This website is under construction"
						span class: "blinking", "_"

					-- Image block
					p ->
						img src: "static/images/jesus_btp.png", alt: "Issou"
