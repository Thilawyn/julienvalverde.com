// Sticks the nav block to the top of the page when scrolled past
document.addEventListener("DOMContentLoaded", function() {
	var nav = document.getElementsByTagName("nav")[0];
	var main = document.getElementsByTagName("main")[0];

	var nav_offset = nav.offsetTop;

	window.addEventListener("scroll", function() {
		if (window.pageYOffset > nav_offset) {
			nav.classList.add("sticky");
			main.classList.add("sticky");
		}
		else {
			nav.classList.remove("sticky");
			main.classList.remove("sticky");
		}
	});
});
