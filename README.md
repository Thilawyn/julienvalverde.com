# julienvalverde.fr

Source code of my personal website hosted at [julienvalverde.fr](http://julienvalverde.fr/).

## Required libraries

- [Lapis](http://leafo.net/lapis/)
- [lapis-systemd](https://github.com/leafo/lapis-systemd)
